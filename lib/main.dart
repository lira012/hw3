import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool isFavorited = true;
  int count = 62;

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('FOOD BATTLE DEBATE!', style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20,),
                  ),
                ),
                Text('Which food item will take the crown as ultimate food?',
                  style: TextStyle(
                    color: Colors.black, fontSize: 15,),
                ),
                Text('\nOne of the most difficult questions to answer is deciding which '
                    'food item is superior over the other - a Sandwich or a slice of pizza? Both have similar qualities. '
                    'They can be made different ways, contain different ingredients, and '
                    'they all have different tastes. I suppose it is just what the person '
                    'consuming the food item prefers! Maybe one day there will be a physical '
                    'battle between these two top dog foods. ONE DAY!', style: TextStyle(
                  color: Colors.black54, fontSize: 13,),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.all(32),
            child: Text('Did you like this post? '
                '\nLet us know by leaving a like!'),
          ),

          Container(
            padding: EdgeInsets.all(0),
            child: IconButton(
              icon: (isFavorited ? Icon(Icons.favorite_border) : Icon(
                  Icons.favorite)), color: Colors.red,
              onPressed: _toggleFavorite,
            ),
          ),
          SizedBox(
            width: 18,
            child: Container(
              child: Text('$count'),
            ),
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'text',
      home: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text('CS481 HW3 - Food Battle Debate!'),
          ),
        ),

        body: ListView(
            children: [
              Image.asset(
                'images/sandwich.jpg',
                width: 600,
                height: 150,
                fit: BoxFit.cover,
              ),
              Image.asset(
                'images/pizza.jpg',
                width: 600,
                height: 150,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection
            ]
        ),
      ),
    );
  }

  void _toggleFavorite() {
    setState(() {
      if (isFavorited) {
        count += 1;
        isFavorited = false;
      } else {
        count -= 1;
        isFavorited = true;
      }
    });
  }
}
